import { Command } from 'ckeditor5/src/core';
import {MODEL_ID_ATTRIBUTE} from './idattributesediting';

class IdAttributesCommand extends Command {

  static documentTreeIds = new Map();

  refresh() {
    const element = this.editor.model.document.selection.anchor.parent;
    this.isEnabled = true;

    if ( !!element && element.hasAttribute( 'customId' ) ) {
      this.value = element.getAttribute( 'customId' );
    } else {
      this.value = false;
    }
  }

  execute( options ) {
    const model = this.editor.model;
    const element = this.editor.model.document.selection.anchor.parent;
    const customId = options.newValue;

    IdAttributesCommand._getDocumentTreeIds(this.editor.model.document.getRoot());
    if (customId === '') {
      IdAttributesCommand.documentTreeIds.delete(this.value);
    }

    if (IdAttributesCommand.documentTreeIds.get(customId) === undefined) {
      model.change(writer => {
        writer.setAttribute('customId', customId, element);
      });
    }
    // @todo Display some kind of warning stating that the specified existed
    //    already if above condition returns false.
  }

  static _getDocumentTreeIds(element) {
    let customId = element.getAttribute( MODEL_ID_ATTRIBUTE );

    if (customId !== undefined && customId !== '') {

      if (this.documentTreeIds.has(customId)) {

        // Retain the element key of the original element containing the ID but
        // increase the count to indicate duplicates.
        this.documentTreeIds.set(customId, {
          count: this.documentTreeIds.get(customId).count + 1,
          elementKey: this.documentTreeIds.get(customId).elementKey
        });
      }
      else {
        this.documentTreeIds.set(customId, {
          count: 1,
          elementKey: this._getElementKey(element)
        });
      }
    }

    if (this.treeDepth === undefined) {
      this.treeDepth = 1;
    }
    else {
      this.treeDepth++;
    }

    // Prevent infinite recursion by limiting depth the IDs are parsed inside
    // nested elements.
    if (this.treeDepth < 99 && element.childCount) {
      for (let i = 0; i < element.childCount; i++) {
        let child = element.getChild(i);
        this._getDocumentTreeIds(child);
      }
    }

    // Reset the recursion depth when execution reaches a top level element.
    if (element.parent === null) {
      this.treeDepth = 0;
    }
  }

  static _getElementKey(element) {
    // The element key consists of a positions index inside the current parent,
    // the position of its parent and so on. This yields a unique key in the DOM.
    let key = element.index;
    if (element.parent.index !== null) {
      key = `${key}.${this._getElementKey(element.parent)}`;
    }
    return key;
  }

}

export default IdAttributesCommand;
