import { Plugin } from 'ckeditor5/src/core';
import IdAttributesCommand from './idattributescommand';

/**
 * The name of the ID attribute.
 */
export const MODEL_ID_ATTRIBUTE = 'customId';

/**
 * The name of the ID attribute.
 */
export const VIEW_ID_ATTRIBUTE = 'id';

class IdAttributesEditing extends Plugin {

  static get pluginName() {
    return 'IdAttributesEditing';
  }

  init() {
    this._registerSchema();
    this._registerConverters();
  }

  _registerSchema() {
    const model = this.editor.model;
    model.schema.extend( '$block', { allowAttributes: [ MODEL_ID_ATTRIBUTE ] } );

    // We need to register a post-fixer here because the 'copyOnEnter' property
    // is not supported on custom attributes, see https://github.com/ckeditor/ckeditor5/issues/8428
    // and an example for it at https://github.com/ckeditor/ckeditor5-list/pull/104
    model.document.registerPostFixer( writer => {
      const changes = model.document.differ.getChanges();

      for ( const entry of changes ) {

        // Some changes don't contain the root which we use to determine all IDs
        // in the document.
        if (entry.position?.root !== undefined) {

          // Re-calculate the IDs with every change, because there may have been
          // IDs added in that change.
          if (IdAttributesCommand.documentTreeIds.size > 0) {
            IdAttributesCommand.documentTreeIds.clear();
          }
          IdAttributesCommand._getDocumentTreeIds(entry.position.root);
        }

        if ( entry.type === 'insert' ) {

          const item = entry.position.nodeAfter;
          const hasCustomId = item !== null && item.hasAttribute( MODEL_ID_ATTRIBUTE );

          if (hasCustomId) {
            const customId = item.getAttribute( MODEL_ID_ATTRIBUTE );
            const hasIdDuplicates = hasCustomId && IdAttributesCommand.documentTreeIds.get(customId).count > 1;

            // Remove the ID attribute if an ID that already existed in the DOM
            // was added to a new element.
            if (hasIdDuplicates && IdAttributesCommand.documentTreeIds.get(customId).elementKey !== IdAttributesCommand._getElementKey(item)) {// && uniqueIds.has(customId)) {
              writer.removeAttribute(MODEL_ID_ATTRIBUTE, item);
            }
          }
        }
      }
    } );
  }

  _registerConverters() {
    const editor = this.editor;

    // Converts the 'customId' model property (attribute) of an any $block
    // model into an 'id' attribute on the corresponding view element.
    editor.conversion.for( 'downcast' ).add( dispatcher =>
      dispatcher.on( `attribute:${ MODEL_ID_ATTRIBUTE }`, ( evt, data, conversionApi ) => {
        if ( !conversionApi.consumable.consume( data.item, evt.name ) ) {
          return;
        }

        const viewWriter = conversionApi.writer;
        const elementView = conversionApi.mapper.toViewElement( data.item );

        if ( data.attributeNewValue !== null && data.attributeNewValue !== '') {
          viewWriter.setAttribute( VIEW_ID_ATTRIBUTE, data.attributeNewValue, elementView );
        } else {
          viewWriter.removeAttribute( VIEW_ID_ATTRIBUTE, elementView );
        }
      } )
    );

    editor.conversion
      .for( 'upcast' )
      .attributeToAttribute( {
        view: VIEW_ID_ATTRIBUTE,
        model: MODEL_ID_ATTRIBUTE
      } );

    this.editor.commands.add( 'IdAttributes', new IdAttributesCommand( this.editor ) );
  }

}

export default IdAttributesEditing;
